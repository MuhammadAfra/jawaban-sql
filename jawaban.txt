1.Buat Database

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.003 sec

2.Membuat Table di Dalam Database 

MariaDB [myshop]> create table categories (
    -> id int auto_increment,
    -> name varchar (50),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.038 sec)

MariaDB [myshop]> describe categories;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(50) | YES  |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
2 rows in set (0.033 sec)

MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(255),
    -> stock int(255),
    -> categories_id int (255),
    -> primary key (id)
    -> );
Query OK, 0 rows affected (0.044 sec)

MariaDB [myshop]> describe items;
+---------------+--------------+------+-----+---------+----------------+
| Field         | Type         | Null | Key | Default | Extra          |
+---------------+--------------+------+-----+---------+----------------+
| id            | int(11)      | NO   | PRI | NULL    | auto_increment |
| name          | varchar(255) | YES  |     | NULL    |                |
| description   | varchar(255) | YES  |     | NULL    |                |
| price         | int(255)     | YES  |     | NULL    |                |
| stock         | int(255)     | YES  |     | NULL    |                |
| categories_id | int(255)     | YES  | MUL | NULL    |                |
+---------------+--------------+------+-----+---------+----------------+
6 rows in set (0.021 sec)

MariaDB [myshop]> create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar (255),
    -> password varchar(255),
    -> primary key (id)
    -> );
Query OK, 0 rows affected (0.045 sec)

MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.024 sec)

3.Memasukkan Data pada Table

MariaDB [myshop]> insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");
Query OK, 5 rows affected (0.004 sec)
MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.000 sec)

MariaDB [myshop]> insert into  items(name, description, price, stock, categories_id) values(
    -> "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1)
    -> ;
Query OK, 3 rows affected (0.006 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+---------------+
| id | name        | description                       | price   | stock | categories_id |
+----+-------------+-----------------------------------+---------+-------+---------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |             1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |             2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |             1 |
+----+-------------+-----------------------------------+---------+-------+---------------+
3 rows in set (0.000 sec)

MariaDB [myshop]> insert into users(name, email, password) values(
    -> "John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.004 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.000 sec)

4.Mengambil Data dari Database

A>
.MariaDB [myshop]> select name, email from users;
+----------+--------------+
| name     | email        |
+----------+--------------+
| John Doe | john@doe.com |
| Jane Doe | jane@doe.com |
+----------+--------------+
2 rows in set (0.001 sec)

B>
b>1.MariaDB [myshop]> select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+---------------+
| id | name        | description                       | price   | stock | categories_id |
+----+-------------+-----------------------------------+---------+-------+---------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |             1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |             1 |
+----+-------------+-----------------------------------+---------+-------+---------------+
2 rows in set (0.001 sec)

b>2.MariaDB [myshop]> select * from items where name like '%Watch';
+----+------------+-----------------------------------+---------+-------+---------------+
| id | name       | description                       | price   | stock | categories_id |
+----+------------+-----------------------------------+---------+-------+---------------+
|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |             1 |
+----+------------+-----------------------------------+---------+-------+---------------+
1 row in set (0.001 sec)

C>
MariaDB [myshop]> select items.name, items.description, items.price, items.stock,items.categories_id, categories.name as kategori from items
    -> inner join categories on items.categories_id = categories.id;
+-------------+-----------------------------------+---------+-------+---------------+----------+
| name        | description                       | price   | stock | categories_id | kategori |
+-------------+-----------------------------------+---------+-------+---------------+----------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |             1 | gadget   |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |             2 | cloth    |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |             1 | gadget   |
+-------------+-----------------------------------+---------+-------+---------------+----------+
3 rows in set (0.001 sec)

5.Mengubah Data dari Database

MariaDB [myshop]> update items set price = 2500000 where id = 1;
Query OK, 1 row affected (0.007 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+---------------+
| id | name        | description                       | price   | stock | categories_id |
+----+-------------+-----------------------------------+---------+-------+---------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |             1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |             2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |             1 |
+----+-------------+-----------------------------------+---------+-------+---------------+
3 rows in set (0.001 sec)
